import { configure, observable, action, runInAction } from 'mobx'

configure({ enforceActions: 'observed' }) // don't allow state modifications outside actions

export default class CurrencyStore {
  @observable.struct
  currencyRates = {
    RUB: 1,
  }

  @observable
  isReady = false

  @observable
  currentCurrency = window.sessionStorage.getItem('currency') || 'RUB'

  @observable
  error = ''

  constructor() {
    this.fetchCurrencyRates()
  }

  @action.bound
  setCurrentCurrency(currency) {
    window.sessionStorage.setItem('currency', currency)
    this.currentCurrency = currency
  }

  @action
  async fetchCurrencyRates() {
    const [USD, EUR] = await fetch(
      'https://prime.exchangerate-api.com/v5/7908d231621e1fbd52bcd2e5/latest/RUB'
    )
      .then(data => data.json())
      .then(currency => [currency.conversion_rates.USD, currency.conversion_rates.EUR])
      .catch(error => {
        runInAction(() => {
          this.error = "Couldn't fetch currency rates"
          console.error(error) // eslint-disable-line
        })
      })

    runInAction(() => {
      this.isReady = true

      this.currencyRates = {
        ...this.currencyRates,
        USD,
        EUR,
      }
    })
  }
}
