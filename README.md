## Frontend test task

- Фильтры сделал через get-параметры
- Курсы валют загружаются с [exchangerate-api.com](https://www.exchangerate-api.com/) + текущая выбранная валюта сохраняется в *sessionStorage*
- Сделал респонсивность до 320px
- *tickets.json* подгружается с локального сервера

До этого не использовал MobX, решил попробовать (мне понравилось :))

### Installation
```sh
npm install
```

### Development

Run development version with dev-server
```sh
npm start
```

Build production version (check 'dist' folder)
```sh
npm run build
```
